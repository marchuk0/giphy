package com.bsa.giphy.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;
import java.util.Map;

@ControllerAdvice
public final class Handler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({UserNotFoundException.class, QueryNotFoundException.class})
    public ResponseEntity<Object> handleNotFoundException(UserNotFoundException ex) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(GifNotFoundException.class)
    public ResponseEntity<Object> handleGifNotFoundException(GifNotFoundException ex) {
        return ResponseEntity.unprocessableEntity().
                body(Map.of("error", ex.getMessage()));
    }
}
