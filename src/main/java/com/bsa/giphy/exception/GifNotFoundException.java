package com.bsa.giphy.exception;

public class GifNotFoundException extends RuntimeException{
    private static final String DEFAULT_MSG = "Gif not found";

    public GifNotFoundException() {
        super(DEFAULT_MSG);
    }

    public GifNotFoundException(String message) {
        super(message);
    }
}
