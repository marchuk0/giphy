package com.bsa.giphy.exception;

public class QueryNotFoundException extends RuntimeException{
    private static final String DEFAULT_MSG = "User not found";

    public QueryNotFoundException() {
        super(DEFAULT_MSG);
    }
}
