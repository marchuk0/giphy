package com.bsa.giphy.exception;

public class UserNotFoundException extends RuntimeException {
    private static final String DEFAULT_MSG = "User not found";

    public UserNotFoundException() {
        super(DEFAULT_MSG);
    }
}
