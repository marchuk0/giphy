package com.bsa.giphy.service;

import com.bsa.giphy.dto.GifUrlDto;
import com.bsa.giphy.dto.GifsDto;
import com.bsa.giphy.dto.HistoryDto;
import com.bsa.giphy.entity.Gifs;
import com.bsa.giphy.entity.History;
import com.bsa.giphy.exception.GifNotFoundException;
import com.bsa.giphy.exception.UserNotFoundException;
import com.bsa.giphy.repository.DiskCacheRepository;
import com.bsa.giphy.repository.DiskUsersRepository;
import com.bsa.giphy.repository.MemoryCacheRepository;
import com.bsa.giphy.util.GifsToDtoMapper;
import com.bsa.giphy.util.HistoryDtoMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class GifUserService {

    private final GifsToDtoMapper gifsToDtoMapper;
    private final HistoryDtoMapper historyDtoMapper;
    private final DiskCacheRepository diskCacheRepository;
    private final MemoryCacheRepository memoryCacheRepository;
    private final DiskUsersRepository diskUsersRepository;
    private final HttpGifUrlService httpGifUrlService;

    public GifUserService(GifsToDtoMapper gifsToDtoMapper,
                           HistoryDtoMapper historyDtoMapper,
                           DiskCacheRepository diskCacheRepository,
                           MemoryCacheRepository  memoryCacheRepository,
                           DiskUsersRepository diskUsersRepository,
                           HttpGifUrlService httpGifUrlService) {
        this.gifsToDtoMapper = gifsToDtoMapper;
        this.historyDtoMapper = historyDtoMapper;
        this.diskCacheRepository = diskCacheRepository;
        this.memoryCacheRepository = memoryCacheRepository;
        this.diskUsersRepository = diskUsersRepository;
        this.httpGifUrlService = httpGifUrlService;
    }

    public List<GifsDto> getAllUserFiles(String id) {
        return gifsToDtoMapper.mapCollection(
                diskUsersRepository.getAllByUser(id));
    }

    public List<HistoryDto> getHistory(String id) {
        return historyDtoMapper.mapCollection(
                diskUsersRepository.getUserHistory(id));
    }

    public void deleteHistory(String id) {
        diskUsersRepository.deleteUserHistory(id);
    }

    public String search(String id, String query, Boolean force) {
        String result = null;
        if(force == null) {
            force = false;
        }
        if(!force) {
            var cached = memoryCacheRepository.findByQuery(id, query);
            if(cached.isPresent()) {
                result = cached.get();
            }
        }
        if(result == null) {
            result = diskUsersRepository.findByQuery(id, query);
        }
        if(result == null) {
            throw new GifNotFoundException("No such gif on server");
        }
        return result;
    }

    public String generate(String id, String query, Boolean force) {
        String result = null;
        if(force == null) {
            force = false;
        }
        if(!force) {
            String path = getGifFromCache(query);
            try {
                if(path != null) {
                    result = diskUsersRepository.copyGifFromPath(path, id, query);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if(result == null) {
            GifUrlDto gifUrl = httpGifUrlService.getGifUrl(query);
            try {
                String from = diskUsersRepository.add(id, query, gifUrl.getUrl(), gifUrl.getId());
                result = from;
                diskCacheRepository.copyGifFromPath(from, query);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        if(result == null) {
            throw new GifNotFoundException("Unable to generate gif");
        }
        memoryCacheRepository.addQuery(id, query, result);
        diskUsersRepository.writeToUserHistory(id, new History(
                new SimpleDateFormat("dd-MM-yyy").format(new Date())
                ,query
                ,result));
        return result;
    }

    private String getGifFromCache(String query) {
        List<Gifs> gifs = diskCacheRepository.findByQuery(query);
        if(gifs.size() == 0) {
            return null;
        }
        Gifs gif = gifs.get(0);
        if(gif.getPaths().size() == 0) {
            return null;
        }
        return gif.getPaths().get(0);
    }

    public void deleteMemoryCache(String name, String query) {
        if(query == null) {
            memoryCacheRepository.removeByName(name);
        }
        else {
            memoryCacheRepository.removeByQuery(name, query);
        }
    }
    public void deleteName(String id) {
        memoryCacheRepository.removeByName(id);
        diskUsersRepository.deleteUserHistory(id);
        diskUsersRepository.deleteUserData(id);
    }

}
