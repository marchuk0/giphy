package com.bsa.giphy.service;

import com.bsa.giphy.dto.GenerateGifRequestDto;
import com.bsa.giphy.dto.GifUrlDto;
import com.bsa.giphy.dto.GifsDto;
import com.bsa.giphy.repository.DiskCacheRepository;
import com.bsa.giphy.util.GifsToDtoMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public final class GifCacheService {

    private final GifsToDtoMapper gifsToDtoMapper;
    private final DiskCacheRepository diskCacheRepository;
    private final HttpGifUrlService httpGifUrlService;

    public GifCacheService(GifsToDtoMapper gifsToDtoMapper,
                           DiskCacheRepository diskCacheRepository,
                           HttpGifUrlService httpGifUrlService) {
        this.gifsToDtoMapper = gifsToDtoMapper;
        this.diskCacheRepository = diskCacheRepository;
        this.httpGifUrlService = httpGifUrlService;
    }

    public List<GifsDto> get(String query) {
        return gifsToDtoMapper.mapCollection(diskCacheRepository.findByQuery(query));
    }

    public GifsDto generate(GenerateGifRequestDto generateGifRequestDto) throws IOException {
            String query = generateGifRequestDto.getQuery();
            GifUrlDto gifUrl = httpGifUrlService.getGifUrl(query);

            String url = gifUrl.getUrl();
            diskCacheRepository.add(query, url, gifUrl.getId());

            return gifsToDtoMapper.mapCollection(diskCacheRepository.findByQuery(query)).get(0);
    }

    public List<String> getAll() {
        return diskCacheRepository.getAll();
    }

    public void delete() {
        diskCacheRepository.delete();
    }
}
