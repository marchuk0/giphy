package com.bsa.giphy.service;

import com.bsa.giphy.dto.GifUrlDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class HttpGifUrlService {
    @Value("${giphy-api.url}")
    private String giphyApiUrl;

    @Value("${giphy-api.key}")
    private String yourApiKey;

    private final HttpClient client;

    @Autowired
    public HttpGifUrlService(HttpClient httpClient) {
        this.client = httpClient;
    }
    public GifUrlDto getGifUrl(String query) {
        try {
            var response = client.send(buildGetRequest(query), HttpResponse.BodyHandlers.ofString());
            return jsonToGifUrlDto(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            return new GifUrlDto("", "");
        }
    }

    private HttpRequest buildGetRequest(String query) {
        var req = HttpRequest
                .newBuilder()
                .uri(URI.create(giphyApiUrl + "?api_key=" + yourApiKey + "&tag=" + query))
                .GET()
                .build();
        return req;
    }

    private GifUrlDto jsonToGifUrlDto(String json) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(json);
        if(jsonNode.get("data") == null || jsonNode.get("data").get("id") == null) {
            return new GifUrlDto("", "");
        }
        return new GifUrlDto(jsonNode.get("data").get("id").asText()
                ,jsonNode.get("data").get("images").get("original_still").get("url")
                .asText().replaceFirst("media\\d", "i"));
    }
}
