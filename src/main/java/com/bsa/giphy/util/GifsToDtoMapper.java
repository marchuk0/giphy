package com.bsa.giphy.util;

import com.bsa.giphy.dto.GifsDto;
import com.bsa.giphy.entity.Gifs;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GifsToDtoMapper {
    public GifsDto map(Gifs gifs) {
        return new GifsDto(gifs.getQuery(), gifs.getPaths());
    }
    public List<GifsDto> mapCollection(List<Gifs> gifsList) {
        return gifsList.stream().map(this::map).collect(Collectors.toList());
    }
}
