package com.bsa.giphy.util;

import com.bsa.giphy.dto.GifsDto;
import com.bsa.giphy.dto.HistoryDto;
import com.bsa.giphy.entity.Gifs;
import com.bsa.giphy.entity.History;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class HistoryDtoMapper {
    public HistoryDto map(History history) {
        return new HistoryDto(history.getDate(), history.getQuery(), history.getPath());
    }
    public List<HistoryDto> mapCollection(List<History> historyList) {
        return historyList.stream().map(this::map).collect(Collectors.toList());
    }
}
