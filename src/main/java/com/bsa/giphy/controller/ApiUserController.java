package com.bsa.giphy.controller;

import com.bsa.giphy.dto.GenerateUserGifDto;
import com.bsa.giphy.dto.GifsDto;
import com.bsa.giphy.dto.HistoryDto;
import com.bsa.giphy.service.GifUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/user/{id:[a-zA-Z0-9]+}")
@Validated
public class ApiUserController {
    private final GifUserService gifUserService;

    @Autowired
    public ApiUserController(GifUserService gifUserService) {
        this.gifUserService = gifUserService;
    }

    @GetMapping("/all")
    public List<GifsDto> queryUserGifsCollection(@PathVariable String id) {
        return gifUserService.getAllUserFiles(id);
    }

    @GetMapping("/history")
    public List<HistoryDto> queryHistory(@PathVariable String id) {
        return gifUserService.getHistory(id);
    }

    @DeleteMapping("/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        gifUserService.deleteHistory(id);
    }

    @GetMapping("/search")
    public String queryUserGif(@PathVariable String id,
                               @RequestParam String query,
                               @RequestParam(required = false) Boolean force) {

        return gifUserService.search(id, query, force);
    }

    @PostMapping("/generate")
    public String generateUserGif(@PathVariable String id, @RequestBody GenerateUserGifDto generateUserGifDto) {
        return gifUserService.generate(id, generateUserGifDto.getQuery(), generateUserGifDto.getForce());
    }

    @DeleteMapping("/reset")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserMemoryCache(@PathVariable String id, @RequestParam(required = false) String query) {
        gifUserService.deleteMemoryCache(id, query);
    }

    @DeleteMapping("/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable String id) {
        gifUserService.deleteName(id);
    }




}
