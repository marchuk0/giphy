package com.bsa.giphy.controller.filters;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(2)
public class HeaderFilter implements Filter {
    private String headerValue = "gg";

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse myResponse = (HttpServletResponse) servletResponse;
        if(httpRequest.getHeader("X-BSA-HEADER") == null ||
                !httpRequest.getHeader("X-BSA-HEADER").equals(headerValue)) {
            myResponse.setStatus(HttpStatus.FORBIDDEN.value());
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
