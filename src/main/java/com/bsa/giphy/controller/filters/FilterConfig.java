package com.bsa.giphy.controller.filters;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FilterConfig {
    @Bean
    public FilterRegistrationBean<UserLoggingFilter> userLoggingFilterFilterRegistrationBean() {
        FilterRegistrationBean<UserLoggingFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new UserLoggingFilter());
        registrationBean.addUrlPatterns("/user/*");

        return registrationBean;
    }
}
