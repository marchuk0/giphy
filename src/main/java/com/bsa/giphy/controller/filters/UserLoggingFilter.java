package com.bsa.giphy.controller.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Order(1)
public class UserLoggingFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger("USER_REQUESTS");


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse myResponse = (HttpServletResponse) servletResponse;
        logger.info("Method: " + httpRequest.getMethod() + " URI:" + httpRequest.getRequestURI());
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
