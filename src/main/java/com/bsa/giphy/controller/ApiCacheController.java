package com.bsa.giphy.controller;

import com.bsa.giphy.dto.GenerateGifRequestDto;
import com.bsa.giphy.dto.GifsDto;
import com.bsa.giphy.service.GifCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;;

@RestController
public final class ApiCacheController {
    private final GifCacheService gifCacheService;

    @Autowired
    public ApiCacheController(GifCacheService gifCacheService) {
        this.gifCacheService = gifCacheService;
    }

    @GetMapping("/cache")
    public List<GifsDto> queryGifsCollection(@RequestParam(required = false) String query) {
        return gifCacheService.get(query);
    }

    @GetMapping("/gifs")
    public List<String> queryPathsToGifsCollection() {
        return gifCacheService.getAll();
    }

    @PostMapping("/cache/generate")
    public GifsDto generateNewGif(@RequestBody GenerateGifRequestDto generateGifRequestDto) throws IOException {
        return gifCacheService.generate(generateGifRequestDto);
    }

    @DeleteMapping("/cache")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCache() {
        gifCacheService.delete();
    }





}
