package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class GenerateUserGifDto {
    private final String query;
    private final Boolean force;
}
