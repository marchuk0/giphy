package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public final class GenerateGifRequestDto {
    private String query;
}
