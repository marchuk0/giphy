package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class HistoryDto {
    private final String date;
    private final String query;
    private final String gif;
}
