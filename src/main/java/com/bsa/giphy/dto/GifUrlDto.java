package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public final class GifUrlDto {
    private String id;
    private String url;
}
