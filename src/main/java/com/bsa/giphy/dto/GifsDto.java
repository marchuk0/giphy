package com.bsa.giphy.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class GifsDto {
    private final String query;
    private final List<String> paths;
}
