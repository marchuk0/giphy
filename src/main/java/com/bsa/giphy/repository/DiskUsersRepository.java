package com.bsa.giphy.repository;

import com.bsa.giphy.entity.Gifs;
import com.bsa.giphy.entity.History;
import com.bsa.giphy.exception.QueryNotFoundException;
import com.bsa.giphy.exception.UserNotFoundException;
import com.bsa.giphy.repository.filefilters.ExtensionFilter;
import com.bsa.giphy.repository.filefilters.QueryFilter;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class DiskUsersRepository {

    private File usersDir;

    public DiskUsersRepository(@Value("${disk.mainpath}") String mainPath) {
        String usersPath = mainPath + "/users";
        this.usersDir = new File(usersPath);
        usersDir.mkdirs();
    }

    public List<History> getUserHistory(String id) {
        File user = getUserFile(id);
        File file = new File(user, "history.csv");
        return parseHistory(file);
    }

    public void writeToUserHistory(String id, History history) {
        File user = getUserFile(id);
        File file = new File(user, "history.csv");
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            bw.write(history.getDate() + "," + history.getQuery() + "," + history.getPath() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteUserHistory(String id) {
        File user = getUserFile(id);
        File file = new File(user, "history.csv");
        file.delete();
    }

    public List<Gifs> getAllByUser(String id) {
        File user = getUserFile(id);
        var queries = Arrays.stream(user.listFiles(new QueryFilter()));
        return queries.map(file -> new Gifs(
                file.getName(),
                Arrays.stream(file.listFiles(new ExtensionFilter(".gif")))
                        .map(gif -> gif.getAbsolutePath()).collect(Collectors.toList())))
                .collect(Collectors.toList());
    }

    public String findByQuery(String id, String query) {
        File user = getUserFile(id);
        File queryDir = new File(user, query);
        if(!queryDir.exists()) {
            throw new QueryNotFoundException();
        }
        File[] files = queryDir.listFiles(new ExtensionFilter(".gif"));
        if(files != null && files.length > 0) {
            return files[0].getAbsolutePath();
        }
        return null;
    }

    public String add(String userId, String query, String url, String id) throws IOException {
        File user = createUserIfAbsent(userId);
        File queryDir = new File(user, query);
        queryDir.mkdir();
        File gif = new File(queryDir, id + ".gif");
        if(!gif.exists()) {
            FileUtils.copyURLToFile(new URL(url), gif);
        }
        return gif.getAbsolutePath();
    }

    public String copyGifFromPath(String srcPath, String userId, String query) throws IOException {
        File src = new File(srcPath);
        File user = createUserIfAbsent(userId);
        File queryDir = new File(user, query);
        queryDir.mkdir();
        File dest = new File(queryDir, src.getName());
        if(!dest.exists()) {
            FileUtils.copyFile(src, dest);
        }
        return dest.getAbsolutePath();
    }

    public void deleteUserData(String id) {
        File user = getUserFile(id);
        for(File file : user.listFiles()) {
            file.delete();
        }
    }

    private List<History> parseHistory(File file) {
        List<History> list = new ArrayList<>();
        String line = "";
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                if(data.length == 3)
                list.add(new History(data[0], data[1], data[2]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    private File getUserFile(String id) throws UserNotFoundException {
        File user = new File(usersDir, id);
        if(!user.exists()) {
            throw new UserNotFoundException();
        }
        return user;
    }

    private File createUserIfAbsent(String id) {
        File user = new File(usersDir, id);
        user.mkdir();
        File history = new File(user, "history.csv");
        if(!history.exists()) {
            try {
                history.createNewFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return user;
    }
}
