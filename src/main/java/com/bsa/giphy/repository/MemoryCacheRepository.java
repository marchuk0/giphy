package com.bsa.giphy.repository;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class MemoryCacheRepository {
    private Map<String, Map<String, List<String>>> cache = new HashMap<>();

    public void removeByName(String name) {
        cache.remove(name);
    }
    public void removeByQuery(String name, String query) {
        if(cache.get(name) != null) {
            cache.get(name).remove(query);
        }
    }
    public Optional<String> findByQuery(String name, String query) {
        if(cache.get(name) == null) {
            return Optional.empty();
        }
        var variants = cache.get(name).get(query);
        if(variants == null || variants.size() == 0) {
            return Optional.empty();
        }
        return Optional.of(variants.get(0));
    }
    public void addQuery(String name, String query, String path) {
        if(!cache.containsKey(name)) {
            cache.put(name, new HashMap<>());
        }
        if(!cache.get(name).containsKey(query)) {
            cache.get(name).put(query, new ArrayList<>());
        }
        cache.get(name).get(query).add(path);
    }



}
