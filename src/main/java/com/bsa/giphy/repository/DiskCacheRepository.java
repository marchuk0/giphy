package com.bsa.giphy.repository;

import com.bsa.giphy.entity.Gifs;
import com.bsa.giphy.repository.filefilters.ExtensionFilter;
import com.bsa.giphy.repository.filefilters.QueryFilter;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class DiskCacheRepository {

    private File cacheDir;

    public DiskCacheRepository (@Value("${disk.mainpath}") String mainPath) {
        String cachePath = mainPath + "/cache";
        this.cacheDir = new File(cachePath);
        cacheDir.mkdirs();
    }
    public List<Gifs> findByQuery(String query)
    {
        cacheDir.mkdirs();
        var queries = Arrays.stream(cacheDir.listFiles(new QueryFilter(query)));
        return queries.map(file -> new Gifs(
                file.getName(),
                Arrays.stream(file.listFiles(new ExtensionFilter(".gif")))
                        .map(gif -> gif.getAbsolutePath()).collect(Collectors.toList())))
                .collect(Collectors.toList());
    }
    public List<String> getAll() {
        cacheDir.mkdirs();
        var queries = Arrays.stream(cacheDir.listFiles(new QueryFilter()));
        return queries.flatMap(file -> Arrays.stream(file.listFiles(new ExtensionFilter(".gif"))))
                .map(gif -> gif.getAbsolutePath())
                .collect(Collectors.toList());
    }

    public String add(String query, String url, String id) throws IOException {
        cacheDir.mkdirs();
        File queryDir = new File(cacheDir, query);
        queryDir.mkdir();
        File gif = new File(queryDir, id + ".gif");
        if(!gif.exists()) {
            FileUtils.copyURLToFile(new URL(url), gif);
        }
        return gif.getAbsolutePath();
    }

    public String copyGifFromPath(String srcPath, String query) throws IOException {
        File src = new File(srcPath);
        cacheDir.mkdirs();
        File queryDir = new File(cacheDir, query);
        queryDir.mkdir();
        File gif = new File(queryDir, src.getName());
        if(!gif.exists()) {
            FileUtils.copyFile(src, gif);
        }
        return gif.getAbsolutePath();
    }


    public void delete() {
        cacheDir.mkdirs();
        for (File query : cacheDir.listFiles()) {
            FileSystemUtils.deleteRecursively(query);
        }
    }
}
