package com.bsa.giphy.repository.filefilters;

import java.io.File;
import java.io.FileFilter;

public final class QueryFilter implements FileFilter {

    private final String query;

    public QueryFilter () {
        this.query = null;
    }
    public QueryFilter (String query) {
        this.query = query;
    }

    @Override
    public boolean accept(File pathname) {
        if(pathname.isFile()) {
            return false;
        }
        if(query == null) {
            return true;
        }
        String name = pathname.getName();
        return name.equals(query);
    }
}