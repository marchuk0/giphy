package com.bsa.giphy.repository.filefilters;

import java.io.File;
import java.io.FileFilter;

public final class ExtensionFilter implements FileFilter {

    private final String extension;

    public ExtensionFilter (String extension) {
        this.extension = extension;
    }

    @Override
    public boolean accept(File pathname) {
        if(pathname.isDirectory()) {
            return false;
        }

        String name = pathname.getName();
        return name.endsWith(extension);
    }
}