package com.bsa.giphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class History {
    private String date;
    private String query;
    private String path;
}
