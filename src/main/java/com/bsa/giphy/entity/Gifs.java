package com.bsa.giphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Gifs {
    private String query;
    private List<String> paths;
}
